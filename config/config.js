module.exports = {
  serverPort: 3000, // listening port
  dbUrl: "mongodb://127.0.0.1:27017", // mongodb url

  jwt: {
    TokenLife: 86400, //life of the token
    secret: "secret123", //the script
  },
};
