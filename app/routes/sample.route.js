const sampleCtlr = require("../controllers/sample.controller.js");
var express = require("express");
var bodyParser = require("body-parser");
var sample = express.Router();
sample.use(bodyParser.json());

sample.route("/login").post(function (req, res, next) {
  sampleCtlr.login(req, res, next);
});

module.exports = sample;
