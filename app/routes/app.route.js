var appRouter = new Object();
const jwtController = require("../controllers/jwt.controller");
const globalHelper = require("../helpers/global.helper");

appRouter.initialize = function (app) {
  const middleware = async (req, res, next) => {
    // return next();
    if (typeof req.headers.token == "undefined" || req.headers.token === null) {
      return res
        .status(400)
        .send({ status: false, message: "Please pass token", data: [] });
    }
    let jwtverify = await globalHelper.jwtVerify(req.headers.token);
    console.log("jwtverify", jwtverify);
    if (jwtverify.status) {
      console.log("Authentication Success");
      return next();
    }
    console.log("Authentication Failed");
    return res
      .status(401)
      .send({ status: false, message: "unauthorized-access", data: [] });
  };

  /*### default route ###*/
  var sample = require("../routes/sample.route");
  app.use("/sample", sample);

  // use middleware for jwt verification
};

module.exports = appRouter;
