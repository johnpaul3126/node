const SampleModel = require("../models/sample.model").sampleSchema;

module.exports = {
  login: async (payload) => {
    try {
      let sample = new SampleModel(payload);
      let result = await sample.save();
      if (result) {
        return { status: true, data: result, message: "ok" };
      }
      return { status: false, data: result, message: "faild to login" };
    } catch (error) {
      return { status: false, data: [], message: error.message };
    }
  },
};
