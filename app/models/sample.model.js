const mongoose = require("mongoose");
const collection = "sample";

const sampleSchema = mongoose.Schema({
  userId: { type: Number, unique: true },
  email: { type: String, unique: true },
  firstName: { type: String },
  lastName: { type: String },
  // default
  status: { type: String },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() },
});

sampleCore.addIncrement(collection, sampleSchema, "EMPLOYEEID", 001, 1, true);

module.exports = {
  sampleSchema: mongoose.model("sample", sampleSchema, "sample"),
};
